var underscore = require("./underscore/underscore-min")

exports.car = function(list) {             
    return list[0];                 
};                                 
exports.cdr = function(list) {             
    return underscore.rest(list);
};                                 
exports.cons = function(atom, list) {      
    var tmp = underscore.toArray(list); 
    tmp.splice(0, 0, atom);         
    return tmp;		     
};                                 

exports.isZero = function(number){
    return number === 0;
};
exports.isNull = function(list) {
    return underscore.isEmpty(list) || underscore.isNull(list);
};

exports.isEq = function(o1, o2) {
    return o1 === o2;	       // The true scheme eq only works on atoms
//    return underscore.isEqual(o1,o2);
};