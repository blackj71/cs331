//Jason Blackwell`
//CS331
//Assignment 2
//2/26/14

//1) make_rn_func 
//random number generator taking in a seed to start computation
var make_rn_func = function (seed) {
	var last = seed;
	var make_rn_func_helper = function (seed, rand) {
         	return "The function is seeded with " + seed + " returns " + rand;
	}
	var compute = function (num) {
        	return (((num * 9)+5)% 1024);
	}		
	return function() {last = compute(last); return make_rn_func_helper(seed,last);};
}

//2) isPrime
//takes a number, returning true if the number is prime and false otherwise
var isPrime = function(number){
    var isPrime_helper = function (n, d) {
    	if(d == 1) {
    	    return true;
		}
		else {
	        if(n % d == 0) {
	            return false;
            }
	        else {
	            return isPrime_helper(n, d - 1);
	    	}
    	}
    }
    }
	 return isPrime_helper(number,number-1);
}

//3) xeroxPrime
//takes a list and returns a new list in which each prime element has been duplicated
var xeroxPrime = function(list){
	if (isNull(list)){
		return [];
	}
	else if (isPrime(car(list)) || isEq(car(list), 2)){
		return cons(car(list), cons(car(list), xeroxPrime(cdr(list))));
	}
	else{
		return cons(car(list), xeroxPrime(cdr(list)));
	}
}

//4) xeroxPrime2
//behaves like xeroxPrime however uses underscore library
var xeroxPrime2 = function(list){
	return underscore.sortBy(list.concat(underscore.filter(list, function(x){if (isPrime(x)) return x;})), function (num) {return num;});	
}

//5) evalPoly
//evaluates a particular polynomial using reduce
var evalPoly = function (list, x) { 
	return underscore.reduce(list, function(memo, num) { return memo * x + num;}, 0);
}

//6) path
//find the path in the particular binary search tree, yet indicates when
// the value is not present in the tree
var path = function (n, bst) {
	var path_helper = function (n, bst, ret){
		if (isNull(bst)){
			return n + " is not in this tree";
		}	
	    if (isEq(n, car(bst))){
        	return ret([]);
	    }
        else if (isLess(n, car(bst))){
            return path_helper(n, car(cdr(bst)),function (t) { return ret(cons(0,t));});
	    }
        else{
            return path_helper(n, car(cdr(cdr(bst))),function (t) {return ret(cons(1,t));});
	    }
	}	
	return path_helper(n,bst,function (x) {return x;});
}

//7) analyze_paths
//receiving a list of tree searches and a function applying reduce
// it returns the length of the shortest, longest, or sum of all paths
var analyze_paths = function (trees, reduce) {
	var analyze_paths_helper = function (n, bst, ret) {	
	 		if (isEq(n, car(bst))){
				return ret(0);
			}
			else if (isLess(n, car(bst))){
				return analyze_paths_helper(n, car(cdr(bst)), function (t) {return ret(t+1);});
			}
			else{
				return analyze_paths_helper(n, car(cdr(cdr(bst))), function (t) {return ret(t+1);});
			}
	}
	var find_all_lengths = function (list, vals) {
	 	if(isNull(list)) {
	 		return vals([]);
	 	}
	  	return find_all_lengths(cdr(list), function(t) {return vals(cons(analyze_paths_helper(car(car(list)), car(cdr(car(list))), function (x) {return x;}), t))});
	}
	return underscore.reduce(find_all_lengths(trees, function (x) {return x;}), reduce);
}

