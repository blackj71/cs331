var underscore = require("./underscore/underscore-min")

var car = function(list) {             
    return list[0];                 
};                                 
var cdr = function(list) {             
    return underscore.rest(list);
};                                 
var cons = function(atom, list) {      
    var tmp = underscore.toArray(list); 
    tmp.splice(0, 0, atom);         
    return tmp;		     
};                                 

var isZero = function(number){
    return number === 0;
};
var isNull = function(list) {
    return underscore.isEmpty(list) || underscore.isNull(list);
};

var isEq = function(o1, o2) {
    return o1 === o2;	       // The true scheme eq only works on atoms
//    return underscore.isEqual(o1,o2);
};

// exports.isMember = function (a, lnums) {
//  if (isNull(lnums)) {
//  return false;
//  } else {
//  return isEq(a, car(lnums) ) || isMember(a, cdr(lnums)) ;
//  }
//  }

var isMember = function (a, lnums) {
if (this.isEq(a, this.car(lnums))) {
return true;
} else if (this.isMember(a, this.cdr(lnums))) {
return true;
} else {
return false;
}
}

 var removeFirst = function (x, l) {
 	if (isNull(l) ) {
 		console.log('return [];');
 		return [];
 	} else if (isEq(x, car(l) )) {
 		console.log('return cdr(l);' + l);
 		return removeFirst(x,cdr(l));
 	} else {
 		console.log('return cons(car(l) , removeFirst(x, cdr(l) ) );' + l);
 		return cons(car(l) , removeFirst(x, cdr(l) ) );
 	}
 }

 var subst = function (n, o, l) {
 if (isNull(l) ) {
 return [];
 } else if (isEq(o, car(l) )) {
 return cons(n, subst(n, o, cdr(l) )) ;
 } else {
 return cons(car(l) , subst(n, o, cdr(l)) );
 }
 }

 module.exports = {
 	car: car,
 	cdr: cdr,
 	cons: cons,
 	isZero: isZero,
 	isNull: isNull,
 	isEq: isEq,
 	isMember: isMember,
 	removeFirst: removeFirst,
 	subst: subst,

 }