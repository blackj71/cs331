//     _  _   ___  ___  _  _   ___ _      _   ___ _  ____      _____ _    _    
//  _ | |/_\ / __|/ _ \| \| | | _ ) |    /_\ / __| |/ /\ \    / / __| |  | |   
// | || / _ \\__ \ (_) | .` | | _ \ |__ / _ \ (__| ' <  \ \/\/ /| _|| |__| |__ 
//  \__/_/ \_\___/\___/|_|\_| |___/____/_/ \_\___|_|\_\  \_/\_/ |___|____|____| //Jason Blackwell
//CS331
//Assignment 1
//2/14/14

//1. Duple
//Returns a list containing n copies of x
var duple = function (n, x){
	if (isZero(n)){
		return [];
	}
	else{	
		return cons(x, duple(sub1(n), x));
	}
}

//2. Reverse
//Reverses the list it is given
var reverse = function (list){
	return reverse_list_helper([], list)
}

var reverse_list_helper = function (revList, origList){
	if(isNull(origList)){
		return revList
	}
	else{
		return reverse_list_helper(cons(car(origList), revList), cdr(origList));
	}	
}
	
//3. Flatten
//Returns a list of the numbers in the list in the order that they occur when list is printed
//Removes all inner parenteses from its argument list
var flatten = function (list){
	return reverse(flatten_list_helper([], list));
}

var flatten_list_helper = function(flatList, origList){
	if(isNull(origList)){
		return flatList;
	}	
	else if (isList(car(origList))){
		return flatten_list_helper(flatten_list_helper(flatList, car(origList)), cdr(origList));
	}	
	else{
		return flatten_list_helper(cons(car(origList), flatList),(cdr(origList)));
	}	
}

//4. Down
//Wraps list brackets around each top-level element of the list l
var down = function (l){
	if (isNull(l)){
		return [];
	}
	else{
		return cons(cons(car(l), []), down(cdr(l)));
	}	
}	

//5. Up
//Removes a pair of brackets from each top-level element of the list l
//If a top-level element is not a list, then it is included in the result as is
var up = function (l){
	return reverse(up_list_helper([], l));
}

var up_list_helper = function (upList, origList){
	if (isNull(origList)){
		return upList
	}
	else if(isNull(car(origList)) && isList(car(origList))){
		return up_list_helper(upList, cdr(origList));
	}	
	else if (isList(car(origList))){
		return up_list_helper(cons(car(car(origList)), upList) ,cons(cdr(car(origList)), cdr(origList)));
	}	
	else{
		return up_list_helper(cons(car(origList), upList), cdr(origList));
	}
}

//6. Path
//n is a number and bst is a binary search tree that contains the number n
//path should return a list of 0s and 1s showing how to find the node containing n
//1 indicates right and 0 indicates left
//if n is found at the root an empty list is returned
var path = function (n, bst){
	if (isEq(n, car(bst))){
		console.log(car(bst));
		return [];
	}
	else if (isLess(n, car(bst))){
		console.log(car(cdr(bst)));
		return cons(0, path(n, car(cdr(bst))));
	}
	else{
		console.log(car(cdr(cdr(bst))));
		return cons(1, path(n, car(cdr(cdr(bst)))));
	}
}
