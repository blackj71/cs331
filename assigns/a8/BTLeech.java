//Jason Blackwell
//Assign 8
//5/15/14
//BTLeech.java
// The BTLeech class you have to write for Assignment 8

// javac -classpath jcspclasses.jar:phwclasses.jar:XtangoAnimation.jar:. BitTorrent.java BTLeech.java

// java -classpath jcspclasses.jar:phwclasses.jar:XtangoAnimation.jar:. BitTorrent antidisestablishmentarianism 16 5 0 400

// The command line parameters can be adjusted to exercise your
// solution (as I will).  They represent the string to be shared, the
// number of leeches, a seed delay factor, a leech delay factor, and a
// general delay factor.

//This implementation randomly selects peers to ask for bits of the string
//peers that it asks will try and get the part of the string if they have it
//a second channel was added to keep track of which peers need to be asked
//a mixPeers method was created to randomly generate a peer to request from
//another channel was added to send the requests to the peers
//the for loop was converted into a while loop to keep the program looping
//if the peer didnt have that part of the string yet

import jcsp.lang.CSProcess;
import jcsp.lang.Channel;
import jcsp.lang.ChannelInput;
import jcsp.lang.ChannelOutput;

class BTLeech
  implements CSProcess
{
  int my_id;
  Channel messages[];
  TheAnimator my_animator;
  int nums[];
  int numsForLetters[];

      // When a leech is constructed, it receives its integer ID (the
    // "processor number"), an array of Any2One channels, and the
    // animator.  This leech receives Messages from other Processors
    // on the channel with its ID number.  It can send Messages to
    // others processors by writing them to the channel identified by
    // that Processor's ID.  Note that, since a leech also receives
    // the animator, it can call methods in its animator.

    public BTLeech(int id, Channel[] message, TheAnimator animator)
    {
        my_id = id;
        this.messages = message;
        this.my_animator = animator;

        nums = new int[BitTorrent.num_leeches + 1];
        numsForLetters = new int[BitTorrent.seed_data.length()];

        for (int i = 0; i < this.numsForLetters.length; i++)
            numsForLetters[i] = i;
        for (int i = 0; i < this.numsForLetters.length; i++){
            int j = (int)(Math.random() * (BitTorrent.seed_data.length() - i)) + i;
            System.out.println(j);
            int k = this.numsForLetters[j];
            numsForLetters[j] = numsForLetters[i];
            numsForLetters[i] = k;
        }
    }

    //generate random peer to try and receive request from
    //not most efficient way but it improves from taking everything from the seed
    private void mixPeers()
    {
        int i = 0;

        for (int j = 0; j < this.nums.length; j++)
            nums[j] = j;
        for (int j = 0; j < this.nums.length; j++){
            int k = (int)(Math.random() * (BitTorrent.num_leeches + 1 - j)) + j;
            int m = this.nums[k];
            nums[k] = nums[j];
            nums[j] = m;
            if (this.nums[j] == 0)
                i = j;
        }
        nums[i] = this.nums[(this.nums.length - 1)];
        nums[(this.nums.length - 1)] = 0;
    }

    //handles
    public void run()
    {
        Channel localChannel2 = this.messages[this.my_id];
        int i = 0;
        int j = numsForLetters[i];
        int k;
        int m = 1;
        while (true){
            Channel localChannel1;
            if ((m != 0) && (i < this.my_animator.length())){
                mixPeers();
                for (k = 0; (k < BitTorrent.num_leeches + 1) && (my_animator.get(this.nums[k], j) == '?'); k++);
                localChannel1 = messages[nums[k]];
                localChannel1.write(new Message(true, this.my_id, j));
            }
            Message localMessage = (Message)localChannel2.read();
            if (!localMessage.request){
                m = 1;

                my_animator.animate_transfer(localMessage.from, my_id, localMessage.char_pos);
                i++;
                if (i < my_animator.length())
                    j = numsForLetters[i];
            }
            else{
                m = 0;
                localChannel1 = messages[localMessage.from];
                localChannel1.write(new Message(false, my_id, localMessage.char_pos));
            }
        }
    }
}
