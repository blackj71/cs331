datatype 'ingredient pizza =
    Bottom
    | Topping of ('ingredient * ('ingredient pizza));

datatype fish =
    Anchovy
    | Shark
    | Tuna;

val my_pizza = Topping(Tuna, Topping(Shark, Topping(Anchovy, Bottom)));

fun rem_anchovy Bottom = Bottom
    | rem_anchovy (Topping(Anchovy,p)) = rem_anchovy(p)
    | rem_anchovy (Topping(t,p)) = Topping(t, rem_anchovy(p));

rem_anchovy my_pizza;

fun rem_ingredient ing = let
	fun rem_ing Bottom = Bottom
   		 | rem_ing (Topping(t,p)) = 
			if ing = t
			then rem_ing(p)
			else Topping(t, rem_ing(p))
	in 
		rem_ing
	end;

datatype tagged = 
	Int of int
	| Real of real
	| Bool of bool
	| String of string;

exception RunTimeTypeError;

fun dynamic_checked_add(Int m, Int n):tagged = 
	let 
		val Int m1 =  Int m
		val Int n1 =  Int n
	in
		Int (m1+n1)
	end
	| dynamic_checked_add(Real m, Real n):tagged = 
	let
		val Real m1 = Real m
		val Real n1 = Real n
	in
		Real (m1+n1)
	end
	| dynamic_checked_add(m:tagged, n:tagged):tagged = 
	raise RunTimeTypeError;

fun dynamic_checked_and(Bool m, Bool n):tagged =
        let
                val Bool m1 = Bool m
                val Bool n1 = Bool n
        in
                Bool (m1 andalso n1)
        end
	| dynamic_checked_and(m:tagged, n:tagged):tagged = 
	raise RunTimeTypeError;

fun dynamic_checked_concatenate(String m, String n):tagged = 
	let
		val String m1 = String m
		val String n1 = String n
	in
		String (m1 ^ n1)
	end
	| dynamic_checked_concatenate(m:tagged, n:tagged):tagged = 
	raise RunTimeTypeError;

fun power_set_helper (a, nil) = nil
	| power_set_helper (a, lst) = (a :: hd lst) :: power_set_helper(a, tl lst); 												
fun power_set(nil) = [nil]
     |   power_set(x::xs) =
             let
                 val y = power_set(xs)
             in
                 y @ power_set_helper(x,y)
             end;

datatype FORMULA =
	ATOMIC of string
	| AND of FORMULA * FORMULA
	| OR of FORMULA * FORMULA
	| NOT of FORMULA;

val my_formula = AND(ATOMIC("x"), OR(ATOMIC("y"),NOT(ATOMIC("y"))));

fun inList(x, []) = false
	| inList(x,L) =
		if (hd L) = x
		then true
		else inList(x,tl L); 

fun eval (ATOMIC(x), L) = (inList(x, L)) 
	| eval (AND (x,y), L) = (eval (x, L)) andalso (eval(y,L)) 
	| eval (OR (x,y), L) = (eval (x, L)) orelse (eval (y,L)) 
	| eval (NOT (x), L) = not (eval(x, L))
		 
 
fun check_formula(ATOMIC(x)) = "neither"
	| check_formula(OR(x,y)) = if check_formula(x) = check_formula(y) then "tautology" else "neither"
	| check_formula(AND(x,y)) = if check_formula(x) = check_formula(y) then "contradiction" else "neither"
	| check_formula(NOT(x)) = "neither"






	
