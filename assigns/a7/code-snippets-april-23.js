// Provide a parameter-less generator function that can be called to
// compute the ith member of the sequence
function Sequence(generator) {
    this.thunk = generator;

    // Call when we want the first n in the sequence
    this.take = function (n) {
        var a = [];
        for (i = 0; i < n; i++) {
            a.push(this.thunk());
        }
        return a;
    };
}


a = new Sequence( function() { return Math.random(); } );

ints_from1 = function() {
    var x = 1;
    return function() {
	var temp = x;
	x = x + 1;
	return temp;
    };
};

ints_from = function(start) {
    return function() {
	var s = start;
	return function() {
	    var temp = s;
	    s = s + 1;
	    return temp;
	};
    };
};

fib = function() {
    var x = 1;
    var y = 1;
    return function() {
	var prev = x;
	x = y;
	y += prev;
	return prev;
    };
};

b = new Sequence( fib() );

x = new Sequence( ints_from1() );


z = new Sequence( (ints_from(5))() );
